'''import nltk # Natural Language Toolkit is a lib whose allow to treat automatically languages
from nltk.stem import WordNetLemmatizer # Lemmatization is the process of converting a word to its base form

import pickle # Serialization manager (same thing as json)
import numpy as np # Calculation lib

from keras.models import load_model # Keras is a deep learning lib
model = load_model('chatbot_model.h5') # Load chat_bot.h5 trained model

import json
import random

def cassandra():

    nltk.download('punkt') # Tokenizer divides a text into a list of sentences, by using an unsupervised algorithm to build a model for abbreviation words
    nltk.download('wordnet') # WordNet module helps to find the meanings of words, synonyms, antonyms


    # Pickle a file from the list
    wordList = ["'s", ',', 'a', 'adverse', 'all', 'anyone', 'are', 'awesome', 'be', 'behavior', 'blood', 'by', 'bye', 'can', 'causing', 'chatting', 'check', 'could', 'data', 'day', 'detail', 'do', 'dont', 'drug', 'entry', 'find', 'for', 'give', 'good', 'goodbye', 'have', 'hello', 'help', 'helpful', 'helping', 'hey', 'hi', 'history', 'hola', 'hospital', 'how', 'i', 'id', 'is', 'later', 'list', 'load', 'locate', 'log', 'looking', 'lookup', 'management', 'me', 'module', 'nearby', 'next', 'nice', 'of', 'offered', 'open', 'patient', 'pharmacy', 'pressure', 'provide', 'reaction', 'related', 'result', 'search', 'searching', 'see', 'show', 'suitable', 'support', 'task', 'thank', 'thanks', 'that', 'there', 'till', 'time', 'to', 'transfer', 'up', 'want', 'what', 'which', 'with', 'you']
    classList = ['adverse_drug', 'blood_pressure', 'blood_pressure_search', 'goodbye', 'greeting', 'hospital_search', 'options', 'pharmacy_search', 'thanks']


    # Pickle Word List
    picklingWords = open("words.pkl","wb")
    pickle.dump(wordList, picklingWords)
    picklingWords.close()
    # Pickle Class List
    picklingClasses = open("classes.pkl","wb")
    pickle.dump(classList, picklingClasses)
    picklingClasses.close()


    # Unpickle Words file
    unpickleWords = open('words.pkl', 'rb')
    readUnpickleWordList = pickle.load(unpickleWords)
    print("\n\nWords", readUnpickleWordList)
    # Unpickle Classes file
    unpickleClasses = open("classes.pkl", "rb")
    readUnpickleClassList = pickle.load(unpickleClasses)
    print("\n\nClasses", readUnpickleClassList, "\n\n")


    # Open pickle and Json files to treat them
    lemmatizer = WordNetLemmatizer()
    intents = json.loads(open('intents.json').read())
    words = pickle.load(open('words.pkl','rb'))
    classes = pickle.load(open('classes.pkl','rb'))



    def clean_up_sentence(sentence):
        # tokenize the pattern - split words into array
        sentence_words = nltk.word_tokenize(sentence)
        # stem each word - create short form for word
        sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
        return sentence_words

    # return bag of words array: 0 or 1 for each word in the bag that exists in the sentence

    def bow(sentence, words, show_details=True):
        # tokenize the pattern
        sentence_words = clean_up_sentence(sentence)
        # bag of words - matrix of N words, vocabulary matrix
        bag = [0]*len(words)
        for s in sentence_words:
            for i,w in enumerate(words):
                if w == s:
                    # assign 1 if current word is in the vocabulary position
                    bag[i] = 1
                    if show_details:
                        print ("found in bag: %s" % w)
        return(np.array(bag))

    def predict_class(sentence, model):
        # filter out predictions below a threshold
        p = bow(sentence, words,show_details=False)
        res = model.predict(np.array([p]))[0]
        ERROR_THRESHOLD = 0.25
        results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
        # sort by strength of probability
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
        return return_list

    def getResponse(ints, intents_json):
        tag = ints[0]['intent']
        list_of_intents = intents_json['intents']
        for i in list_of_intents:
            if(i['tag']== tag):
                result = random.choice(i['responses'])
                break
        return result

    def chatbot_response(msg):
        ints = predict_class(msg, model)
        res = getResponse(ints, intents)
        return res'''
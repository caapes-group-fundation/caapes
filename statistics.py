# coding=utf-8

import datetime


def statistics():
    # Catch from database with ORM request
    number_user = 3
    number_message = 100
    max_message_username = 'ChequerDuLac'
    max_message = 56

    # Example of datetime format
    # Catch the current datetime
    date_now = datetime.datetime.now().year
    # Example of returned list from ORM
    number_message_date = [datetime.datetime(2007, 5, 2, 2, 8, 52, 29000),
                           datetime.datetime(2020, 6, 8, 4, 6, 20, 50000),
                           datetime.datetime(2015, 9, 5, 5, 1, 12, 12000),
                           datetime.datetime(2006, 3, 7, 6, 2, 16, 36000)]
    # Calculate the mean sent message hour
    mean_message_per_hour = (number_message_date[0].hour + number_message_date[1].hour + number_message_date[2].hour +
                             number_message_date[3].hour) / 4
    # Calculate the mean sent message day
    mean_message_per_day = (number_message_date[0].day + number_message_date[1].day + number_message_date[2].day +
                            number_message_date[3].day) / 4
    print("hours :", number_message_date[0].hour, "  ", number_message_date[1].hour, "  ", number_message_date[2].hour,\
        "  ", number_message_date[3].hour, "  ", mean_message_per_hour)
    print("days  :", number_message_date[0].day, "  ", number_message_date[1].day, "  ", number_message_date[2].day,\
        "  ", number_message_date[3].day, "  ", mean_message_per_day)

    # Determined by calc
    mean_message_per_user = number_user / number_message

    # Will be used to fill logs files
    print("number of user            :", number_user)
    print("number of messages        :", number_message)
    print("most frequent publisher   :", max_message_username)
    print("max message number user   :", max_message)
    print("mean message per user     :", mean_message_per_user)


statistics()

DROP DATABASE IF EXISTS caapes;

CREATE DATABASE caapes;

USE caapes;


CREATE TABLE `users`(
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` varchar(255) NULL,
    `password` varchar(255) NULL,
    `priv` varchar(5) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `invite_tokens`(
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `token` varchar(255) NULL,
    `priv` varchar(5) NOT NULL,
    `validity` BOOLEAN NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `chanels`(
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(255) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `chanel_messages`(
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `message`varchar(255) NOT NULL,
    `chanel_id` int(10) UNSIGNED NOT NULL,
    FOREIGN KEY (chanel_id) REFERENCES chanels(id))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `client_pub_key`(
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `public_key` varchar(20000) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `server_pub_key`(
    `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `public_key` varchar(20000) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO users(username, password, priv) VALUES
("eliot", "superadmin", "admin"),
("mr.robot", "superadmin", "admin"),
("tyrell", "superadmin", "admin"),
("gedeon", "gedeon", "user");



INSERT INTO chanels(name) VALUES
("admin"),
("générale");




INSERT INTO invite_tokens(token, priv, validity) VALUES
("@f-ZV55CW#bfa3D[)eD]=(bFO<:k!.[,J-_0l+})|w1M3O}r&vv6WEw1WzuM1F!M", "user", true)

INSERT INTO client_pub_key(public_key) VALUES
("-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAp8bxX5RJcHQUJpXzkxsU
wTJ0B5L0xpj3lv9m+BzQFA60c/16zewQYXZmvdYBCOc2alEtekb+ymyBmSMntIhs
oO1YojLkyH+39BGOlo3v5ntrNAyUMczJa289MQOHIXppUngDh3pZieP40d/ekfgn
dSkFvV3y1b8yzkokGwedOZzrKXnBpwjGSq996hzLTNk9qnjDMQ54OqJ5891+aATV
+NC4zYuAWUVyBCr/l5NIOJ6xiHw95Mjvoilzn1bsTfnmZXumMins6u7gCpx0KSZ+
/67KHXCoy8qKhIyOPZd1ce42HHsF0MOh12XPfw2gZRZympRybErt8lf6eLf1cQkn
Iz3CYS7OeRQT/V8MGBLITm7nbXdR3Pz92e5wz/H5Ff1a4unlLpsgzqdIYZUJZoCP
Rr59Z5p5JnCrDdvWLNotiOZEk0t+emIHXNEZ2w5mrMIVjcPkIkMj+IpA1O+UMgtd
eYA383Jio/+O9D8s5A2W4IjFZ49I76egKNWPoC9sIYmGJUGZVPnScnIbcxLsKlcK
MAty5F9oNP0r4HkcmcDwUG5oc0ad48n0HNajyPN+G0hFaBSGkgKBpR7fAgDOvLjJ
MXPkGKZ3YVM9qvasyFZxE4uqSs9VgYT4TyzjfhCjx1FEf9IWS/gko8J1BhvozzQs
CpnQDYLLxP9V/bOj86muXpMCAwD//w==
-----END PUBLIC KEY-----")
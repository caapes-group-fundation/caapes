#!usr/bin/env/python3
import os
import socket
import select
import sys
import json
import time
from random import uniform

from caapesClientEncryptio import ClientEncryption
from conf import conf  # Import file configuration
from chatFunctions import Function
from tqdm import tqdm  # Use to make loading bar

class CapesClient:

    def __init__(self):
        # Use socket to instantiate a server socket object for init connection
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Define external Port and IP
        self.ip_address = conf.CLIENT_IP_ADDRESS
        self.port = conf.PORT
        # Connect the server to the bind endpoint
        self.server.connect((self.ip_address, self.port))
        # init encryption module
        self.capes_encryptio = ClientEncryption()
        self.capes_encryptio.iniAndtStoreKey()



    def jsonSend(self, msg):
        jsonData = json.dumps(msg).encode('utf-8')
        self.server.send(bytes(jsonData))

    def jsonRecv(self):
        data = self.server.recv(2048)
        jsonData = bytes.decode(data)
        msg = json.loads(jsonData)
        return msg

    # Progress bar loading when we start
    loop = tqdm(total = 100, position=0, leave=False, unit=' Fsociety...')
    for k in range(100):
        loop.set_description('Loading Caapes...'.format(k))
        loop.update(1)
        time.sleep(uniform(0.07, 0.01))
    loop.close()

    def initClient(self):
        while True:
            # maintains a list of possible input streams
            sockets_list = [sys.stdin, self.server]

            # Init each variable with select statement
            read_sockets, write_socket, error_socket = select.select(sockets_list, [], [])

            # Received the message from each socket in the same chat canal
            for socks in read_sockets:
                if socks == self.server:
                    msg = self.jsonRecv()
                    print(msg)
                else:
                    inptMsg = sys.stdin.readline().rstrip('\n')
                    self.jsonSend(inptMsg)
                    '''data = bytes(inptMsg, 'utf-8')
                    server.send(data)'''
                    if inptMsg == "@ascii":
                        sys.stdout.write("<moi>")
                        break
                    if inptMsg == "@screen":
                        function = Function()
                        function.screenshot()
                        break
                    if inptMsg == "@readFile":
                        function = Function()
                        function.readFile()
                        break
                    if inptMsg == "@clear":
                        function = Function()
                        function.clearTerminal()
                        break
                    else:
                        sys.stdout.flush()
                        # if msg == "@list":
                        #server.send(bytes(inptMsg, 'ascii'))

                        #print("<moi>" + inptMsg)

caapesClient = CapesClient()
caapesClient.initClient()
caapesClient.server.close()

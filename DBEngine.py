from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:''@127.0.0.2/caapes'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class CapesDB:

    def __init__(self):
        self.db = db


    def getUserByLogin(self, username, passwd):
        result = self.db.session.execute('SELECT * FROM users WHERE username= :val AND password= :val2', {'val': username, 'val2': passwd})
        sql_data = result.fetchall()
        raw_count = len(sql_data)
        if raw_count == 0:
            return False
        else:
            return sql_data


    def insertUser(self, username, passwd, priv):
        query = Users(username, passwd, priv)
        self.db.session.add(query)
        self.db.session.commit()


    def insertChanel(self, name):
        query = Chanels(name)
        self.db.session.add(query)
        self.db.session.commit()



    def getAllChanels(self):
        result = self.db.session.execute('SELECT * from chanels')
        sql_data = result.fetchall()
        raw_count = len(sql_data)
        if raw_count == 0:
            return False
        else:
            return sql_data



    def insertMessage(self, chanel_id, content):
        query = ChanelMessage(chanel_id, content)
        self.db.session.add(query)
        self.db.session.commit()


    def getAllMessageByChanel(self, chanel_id):
        result = self.db.session.execute('SELECT * from chanel_messages WHERE chanel_id= :val', {'val': chanel_id})
        sql_data = result.fetchall()
        raw_count = len(sql_data)
        if raw_count == 0:
            return False
        else:
            return sql_data




    def getInviteToken(self, token):
        result = self.db.session.execute('SELECT * from invite_tokens WHERE token = :val', {'val': token})
        sql_data = result.fetchall()
        raw_count = len(sql_data)
        if raw_count == 0:
            return False
        else:
            return sql_data


    def insertInviteToken(self, token, priv, validity):
        query = InviteTokens(token, priv, validity)
        self.db.session.add(query)
        self.db.session.commit()



    def getClientPubKey(self):
        result = self.db.session.execute('SELECT client_pubic_key from key')
        sql_data = result.fetchall()
        raw_count = len(sql_data)
        if raw_count == 0:
            return False
        else:
            return sql_data


    def insertClientPublicKey(self, key):
        query = ClientPubKey(key)
        self.db.session.add(query)
        self.db.session.commit()



    def getServerPubKey(self):
        result = self.db.session.execute('SELECT server_pubic_key from key')
        sql_data = result.fetchall()
        raw_count = len(sql_data)
        if raw_count == 0:
            return False
        else:
            return sql_data


    def insertServerPublicKey(self, key):
        query = ServerPubKey(key)
        self.db.session.add(query)
        self.db.session.commit()





class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, unique=False)
    priv = db.Column(db.Boolean, nullable=False, unique=False)

    def __init__(self, username, password, priv):
        self.username = username
        self.password = password
        self.priv = priv


class Chanels(db.Model):
    __tablename__ = 'chanels'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False, unique=True)

    def __init__(self, name):
        self.name = name


class ChanelMessage(db.Model):
    __tablename__ = 'chanel_messages'
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(500), nullable=True, unique=False)
    chanel_id = db.Column(db.Integer, db.ForeignKey("chanels.id"), nullable=False)

    def __init__(self, chanel_id, message):
        self.chanel_id = chanel_id
        self.message = message


class InviteTokens(db.Model):
    __tablename__ = 'invite_tokens'
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(255), nullable=True, unique=True)
    priv = db.Column(db.String(8), nullable=False, unique=False)
    validity = db.Column(db.Boolean, nullable=False, unique=False)

    def __init__(self, token, priv, validity):
        self.token = token
        self.priv = priv
        self.validity = validity



class ClientPubKey(db.Model):
    __tablename__ = 'client_pub_key'
    id = db.Column(db.Integer, primary_key=True)
    public_key = db.Column(db.String(255), nullable=True, unique=True)

    def __init__(self, public_key):
        self.token = public_key


class ServerPubKey(db.Model):
    __tablename__ = 'server_pub_key'
    id = db.Column(db.Integer, primary_key=True)
    public_key = db.Column(db.String(255), nullable=True, unique=True)

    def __init__(self, public_key):
        self.token = public_key








'''db1 = CapesDB()
user = "admin"
passwd = "superadmin"
token = "@f-ZV55CW#bfa3D[)eD]=(bFO<:k!.[,J-_0l+})|w1M3O}r&vv6WEw1WzuM1F!M"

#db1.insertInviteToken(token, 'user', True)
#db1.insertUser(user, passwd, True)


db1.insertChanel('générale')
db1.insertMessage(2, "ceci est mon message 1")
#db1.insertClientPublicKey("")

user_data = db1.getUserByLogin(user, passwd)
print(user_data)
token_data = db1.getInviteToken(token)
print(token_data)
all_mess = db1.getAllMessageByChanel("admin")
all_channel = db1.getAllChanels()
print(all_mess)
print(all_channel)'''
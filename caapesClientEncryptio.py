#!usr/bin/env/python3
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.fernet import Fernet
from os import path


class ClientEncryption:


    def __init__(self):
        self.public_exponent = 65535
        self.key_size = 4096
        self.backend = default_backend()
        self.encoding = serialization.Encoding.PEM
        self.format = serialization.PrivateFormat.PKCS8
        self.encryption_algo = serialization.NoEncryption()
        self.private_key = rsa.generate_private_key(self.public_exponent, self.key_size, self.backend)
        self.public_key = self.private_key.public_key()
        #self.encrypt_sym_key = self.readSymKey()
        #self.sym_key = self.decrypSymKey(self.encrypt_sym_key)
        self.sym_key = 'wQDR9cKdAytISP1wXUClyw09R6WJeNaWxzXnQ20r56U='



    def iniAndtStoreKey(self):

        if not path.exists('c_private_key.pem'):
            pv_pem = self.private_key.private_bytes(self.encoding, self.format, self.encryption_algo)
            pu_pem = self.public_key.public_bytes(encoding=serialization.Encoding.PEM, format=serialization.PublicFormat.SubjectPublicKeyInfo)

            with open('c_private_key.pem', 'wb') as file:
                file.write(pv_pem)

            with open('c_public_key.pem', 'wb') as file:
                file.write(pu_pem)


    def decrypSymKey(self, data):
        sym_key = self.asynDecrypt(data)
        return sym_key


    def readSymKey(self):
        with open('encrypted_sym', 'r') as key_file:
            encrypt_sym_key = key_file.read()
        return encrypt_sym_key


    def readPublicKey(self):
        with open('c_public_key.pem', 'rb') as key_file:
            public_key = serialization.load_pem_public_key(key_file.read(), backend=default_backend())
        return public_key


    def readPrivateKey(self):
        with open('c_private_key.pem', 'rb') as key_file:
            private_key = serialization.load_pem_private_key(key_file.read(), password=None, backend=default_backend())
        return private_key


    def asynEncrypt(self, data):
        public_key = self.readPublicKey()
        encrypted = public_key.encrypt(data, padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()), algorithm=hashes.SHA256(), label=None))
        return encrypted


    def asynDecrypt(self, data):
        private_key = self.readPrivateKey()
        decrypted = private_key.decrypt(data, padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()), algorithm=hashes.SHA256(), label=None))
        return decrypted


    def symEncrypt(self, data):
        cipher = Fernet(self.sym_key)
        formated_data = bytes(data, 'utf-8')
        sym_crypted_data = cipher.encrypt(formated_data)
        return sym_crypted_data



    def symDecrypt(self, data):
        cipher = Fernet(self.sym_key)
        sym_decrypted_data = cipher.decrypt(data)
        return sym_decrypted_data




'''CEncrypt = CapesEncryption()
CEncrypt.iniAndtStoreKey()
msg = "ceci est un message sensible"
encrypted = CEncrypt.symEncrypt(msg)
print(encrypted)
decrypted = CEncrypt.symDecrypt(encrypted)
print(decrypted)'''


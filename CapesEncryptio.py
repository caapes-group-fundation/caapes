#!usr/bin/env/python3
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.fernet import Fernet
import os


class CapesEncryption:

    def __init__(self):
        self.public_exponent = 65535
        self.key_size = 4096
        self.backend = default_backend()
        self.encoding = serialization.Encoding.PEM
        self.format = serialization.PrivateFormat.PKCS8
        self.encryption_algo = serialization.NoEncryption()
        self.private_key = rsa.generate_private_key(self.public_exponent, self.key_size, self.backend)
        self.public_key = self.private_key.public_key()
        #self.sym_key = Fernet.generate_key()
        #print(self.sym_key)
        self.sym_key = 'wQDR9cKdAytISP1wXUClyw09R6WJeNaWxzXnQ20r56U='



    def iniAndtStoreKey(self):

        if not os.path.exists('private_key.pem'):
            pv_pem = self.private_key.private_bytes(self.encoding, self.format, self.encryption_algo)
            pu_pem = self.public_key.public_bytes(encoding=serialization.Encoding.PEM, format=serialization.PublicFormat.SubjectPublicKeyInfo)

            with open('private_key.pem', 'wb') as file:
                file.write(pv_pem)

            with open('public_key.pem', 'wb') as file:
                file.write(pu_pem)


    def readPublicKey(self, key):
        with open(key, 'rb') as key_file:
            public_key = serialization.load_pem_public_key(key_file.read(), backend=default_backend())
        return public_key


    def readClientPublicKey(self, key):
        with open(key, 'rb') as key_file:
            public_key = serialization.load_pem_public_key(key_file.read(), backend=default_backend())
        return public_key


    def readPrivateKey(self):
        with open('private_key.pem', 'rb') as key_file:
            public_key = serialization.load_pem_private_key(key_file.read(), password=None, backend=default_backend())
        return public_key


    def asynEncrypt(self, data, public_key):
        public_key = self.readClientPublicKey(public_key)
        encrypted = public_key.encrypt(data, padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()), algorithm=hashes.SHA256(), label=None))
        return encrypted


    def asynDecrypt(self, data):
        private_key = self.readPrivateKey()
        decrypted = private_key.decrypt(data, padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA256()), algorithm=hashes.SHA256(), label=None))
        return decrypted


    def symEncrypt(self, data):
        cipher = Fernet(self.sym_key)
        formated_data = bytes(data, 'utf-8')
        sym_crypted_data = cipher.encrypt(formated_data)
        return sym_crypted_data



    def symDecrypt(self, data):
        cipher = Fernet(self.sym_key)
        sym_decrypted_data = cipher.decrypt(data)
        return sym_decrypted_data




CEncrypt = CapesEncryption()
CEncrypt.iniAndtStoreKey()
'''msg = "ceci est un message sensible"
encrypted = CEncrypt.symEncrypt(msg)
print(encrypted)
decrypted = CEncrypt.symDecrypt(encrypted)
print(decrypted)'''

#!usr/bin/env/python3
import os
import sys
from conf import conf  # File configuration
from colorama import *
import pyscreenshot as ImageGrab  # Use to take a screenshot

class Function:

    def __init__(self):
        self.invalidPath = 'Chemin non valide\n'

    def screenshot(self):
        # Title of the screenshot mode
        sys.stdout.write("\n" + conf.DARK_GREEN + conf.SCREENSHOT_TITLE + "\n" + Style.RESET_ALL)
        # Asks for the path where the file will be saved
        path = input(conf.DARK_GREEN + conf.SCREENSHOT_PATH + Style.RESET_ALL)
        # Asks for the name of file
        name = input(conf.DARK_GREEN + conf.SCREENSHOT_NAME + Style.RESET_ALL)

        try:
            # Take the screenshot
            image = ImageGrab.grab()
            # Save the screenshot
            image.save(path + name + '.png')
            # Return success
            sys.stdout.write(conf.DARK_GREEN + conf.SCREENSHOT_SUCCESS + path + name + '.png\n\n' + Style.RESET_ALL)
        except:
            # return error
            sys.stdout.write(self.invalidPath)

    def readFile(self):
        # Title of the readFile mode
        sys.stdout.write("\n" + conf.DARK_GREEN + conf.READFILE_TITLE + "\n" + Style.RESET_ALL)
        # Ask for the path to the file to read
        path = input(conf.DARK_GREEN + conf.READFILE_PATH + Style.RESET_ALL)
        # Asks for the name of file
        name = input(conf.DARK_GREEN + conf.READFILE_NAME + Style.RESET_ALL)

        try:
            # Open the file on read mode
            with open(path + name + ".txt", "r") as file:
                print(file.read())
        except:
            # Return error invalid path
            sys.stdout.write(conf.YELLOW + conf.READFILE_INVALID_PATH + "\n\n" + Style.RESET_ALL)

    def clearTerminal(self):
        os.system('cls' if os.name == 'nt' else 'clear')


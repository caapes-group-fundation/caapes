from colorama import *
# File to use for variable configuration

# Configuration server
IP_ADDRESS = '********'
CLIENT_IP_ADDRESS = '*******'
PORT = 6699

###############################
# Configure Anchors           #
###############################

# Server Anchor
PROCESS_SUCCESS_ANCHOR = '[OK] '
PROCESS_FAILURE_ANCHOR = '[ERROR] '

# User Anchor
INFO_ANCHOR = '[i] '
ERROR_ANCHOR = '[-] '
LOADING_ANCHOR = '[*] '
SUCCESS_ANCHOR = '[+] '
WARNING_ANCHOR = '[!] '
CURSOR_ANCHOR = '>  '

###############################
# Custom Colors               #
###############################
GREEN = '\033[92;1m'
DARK_GREEN = '\033[32;1m'
RED = '\033[31;1m'
YELLOW = '\033[93;1m'
WHITE = '\033[37m'

###############################
# Server Message Connection   #
###############################

PORT_LISTEN = PROCESS_SUCCESS_ANCHOR + "Listen on port "
SERVER_START = PROCESS_SUCCESS_ANCHOR + "Server start"
WAITING_CONNEXION = LOADING_ANCHOR + "Waiting for incoming connexion"

###############################
# Client Message Connection   #
###############################

LOGIN_LABEL = "Username for 'caapes': "
PSWD_LABEL = "Password for 'caapes': "


###############################
# Client Connection Error     #
###############################

LOGIN_ERROR = WARNING_ANCHOR + "Pas d'utilisateur correspondant"

PRIVATE_ERROR = WARNING_ANCHOR + "Vous ne Passserez paaaas !\n" + \
                INFO_ANCHOR + "Cette section réservé aux admin !"

LOGIN_ERROR_OUT = WARNING_ANCHOR + "Vos tentative de connexion sont dépassées\n" + \
                  ERROR_ANCHOR + "Deconnexion du serveur"

###############################
# Client Informative Labels   #
###############################
PANEL = "\n" + SUCCESS_ANCHOR + "Bienvenue sur CAAPES\n" + \
        CURSOR_ANCHOR + "00 Canal admin\n" + \
        CURSOR_ANCHOR + "01 Canal général\n\n"

INFO_CHANEL = INFO_ANCHOR + "Vous avez bien rejoins le canal "

# List to display a random ascii message when you write @ascii
LIST_ASCII = ["(╯°□°）╯︵ ┻━┻", "┻━┻ ︵ヽ(`Д´)ﾉ︵﻿ ┻━┻", "¯\_(ツ)_/¯", "( ͡° ͜ʖ ͡°)", "ʘ‿ʘ", "°‿‿°",
             "ヽ(´ー｀)ノ", "щ（ﾟДﾟщ）", "[¬º-°]¬", "ʕʘ̅͜ʘ̅ʔ", "ʕ•ᴥ•ʔ", "(`･ω･´)", "┬─┬⃰͡ (ᵔᵕᵔ͜ )",
             "(ノಠ ∩ಠ)ノ彡( \o°o)\ ", "(∩｀-´)⊃━☆ﾟ.*･｡ﾟ", "ε=ε=ε=┌(;*´Д`)ﾉ"]


################################
# Chat Functions Configuration #
################################

# screenshot function
SCREENSHOT_TITLE = INFO_ANCHOR + "Mode screenshot  " + INFO_ANCHOR
SCREENSHOT_PATH = "Entrez le chemin de sauvegarde de votre capture(exemple : /home/screenshot/) :"
SCREENSHOT_NAME = "Entrez le nom de votre capture (sans l'extension) :"
SCREENSHOT_SUCCESS = "Votre screenshot est enregistré : "

# readFile function
READFILE_TITLE = INFO_ANCHOR + "Mode lecture de fichier texte  " + INFO_ANCHOR
READFILE_PATH = "Entrez le chemin de votre fichier texte(exemple : /home/dossier/) :"
READFILE_NAME = "Entrez le nom de votre fichier (sans l'extension) :"
READFILE_INVALID_PATH = "Chemin ou nom du ficher non valide"

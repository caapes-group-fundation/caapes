#!usr/bin/env/python3
import os
import base64
import threading
import socket
import json
from _thread import *
from conf import conf
from chatFunctions import Function  # Chat feature queue
from random import *  # Use to send a random ascii
from graphicOverload import userInterface, adminInterface
from colorama import *
from CapesEncryptio import CapesEncryption
from DBEngine import *



class CaapesServer:

    def __init__(self):
        # Create a client list and client IP list  variable to stock each client
        self.clientList = []
        self.clientIPList = []
        # Create an admin and admin IP list variable tock stock each client
        self.clientAdminList = []
        self.clientAdminIpList = []
        self.admin = False
        self.user = False

        # Define Port and local IP
        self.ip_address = conf.IP_ADDRESS
        self.port = conf.PORT

        # Use socket to instantiate a server socket object for init connection
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Start encryption module
        self.ccryptor = CapesEncryption()
        self.ccryptor.iniAndtStoreKey()
        # bind to these current IP and port
        self.server.bind((self.ip_address, self.port))
        print(conf.GREEN + conf.SERVER_START + Style.RESET_ALL)
        # server is setup to listen 100 connection max
        self.server.listen(100)
        print(conf.GREEN + conf.PORT_LISTEN + str(self.port) + Style.RESET_ALL)
        print(conf.WHITE + conf.WAITING_CONNEXION + Style.RESET_ALL)
        # Define class Function
        self.function = Function()
        # Import DB class
        self.caapes_DB = CapesDB()

    # Check and remove connection if dead
    # Rename in clientRemove
    def removeClient(self, connection):
        if connection in self.clientList:
            self.clientList.removeClient(connection)

    # To redefine in one
    # send a broadcast (message to all user)
    def clientBroadcast(self, message, connection):
        for clients in self.clientList:
            if clients != connection:
                try:
                    jsonData = json.dumps(message).encode('utf-8')
                    clients.send(bytes(jsonData))
                except:
                    clients.close()

                    # if the link is broken, we remove the client
                    self.removeClient(clients)

    # send a broadcast (message to all user)
    def adminBroadcast(self, message, connection):
        for adminClient in self.clientAdminList:
            if adminClient != connection:
                try:
                    jsonData = json.dumps(message).encode('utf-8')
                    adminClient.send(bytes(jsonData))
                except:
                    adminClient.close()

                    # if the link is broken, we remove the client
                    self.removeClient(adminClient)

    def jsonSend(self, conn, msg):
        jsonData = json.dumps(msg).encode('utf-8')
        conn.send(bytes(jsonData))


    def jsonRecv(self, conn):
        data = conn.recv(2048)
        jsonData = bytes.decode(data)
        msg = json.loads(jsonData)
        return msg

    '''def checkPrivilege(self, user):
        if user == "admin":
            return True
        else:
            return False'''

    def checkLogin(self, conn, addr):
        # put do while loop
        connection_attempt = 0
        # global admin
        while connection_attempt < 3:
            self.jsonSend(conn, "\n" + conf.DARK_GREEN + conf.LOGIN_LABEL + Style.RESET_ALL)
            userLog = self.jsonRecv(conn)
            self.jsonSend(conn, "\n" + conf.DARK_GREEN + conf.PSWD_LABEL + Style.RESET_ALL)
            passLog = self.jsonRecv(conn)
            # make a refer to ORM boolean variable
            userLogin = self.caapes_DB.getUserByLogin(userLog, passLog)
            print(userLogin)
            print(userLogin[0][2])
            if userLogin != False:
                if userLogin[0][3] == "admin":
                    # simplify passwd
                    # use to identify current user
                    self.admin = True
                    print("okokok")
                    break

                elif userLogin[0][3] == "user":
                    self.user = True
                    break
                else:
                    self.jsonSend(conn, "\n" + conf.YELLOW + conf.LOGIN_ERROR + Style.RESET_ALL)
                    connection_attempt += 1
                    print("okokok2")
                    continue

        if connection_attempt == 3:
            self.jsonSend(conn, conf.RED + conf.LOGIN_ERROR_OUT + Style.RESET_ALL)
            return False
        else:
            return True

    def initSession(self, conn, addr):
        if self.checkLogin(conn, addr):
            self.jsonSend(conn, conf.DARK_GREEN + conf.PANEL + Style.RESET_ALL)
            # keep  the connection open
            while True:
                try:
                    data = self.jsonRecv(conn)
                    # print(bytes.decode(data))
                    print(data)
                    if data[:2] == "cd" and data[3:] == "admin" and self.admin == True:
                        chanel_id = 1
                        self.clientAdminList.append(conn)
                        self.clientAdminIpList.append(conn)
                        self.jsonSend(conn, conf.WHITE + conf.INFO_CHANEL + "Admin\n" + Style.RESET_ALL)
                        while True:
                            msg = self.jsonRecv(conn)
                            if msg == "@exit":
                                self.jsonSend(conn, conf.DARK_GREEN + conf.PANEL + Style.RESET_ALL)
                                self.removeClient(conn)
                                break
                            if msg == "@screen":
                                print('screen effectué : ' + addr[0])
                            if msg == "@readFile":
                                print('readFile effectué : ' + addr[0])
                            if msg == "@ascii":
                                message_ascii = choice(conf.LIST_ASCII)
                                message_to_send = "<" + addr[0] + "> " + message_ascii
                                self.adminBroadcast(message_to_send, conn)
                                self.jsonSend(conn, message_ascii + '\n')
                            if msg == "@graphicmod":
                                adminInterface.graphicOverload()
                            else:
                                message_to_send = "<" + addr[0] + "> " + msg
                                crypted_msg = self.ccryptor.symEncrypt(message_to_send)
                                print(crypted_msg)
                                self.adminBroadcast(message_to_send, conn)
                                self.caapes_DB.insertMessage(chanel_id, crypted_msg)

                    elif data[:2] == "cd" and data[3:] == "admin" and self.user == True:
                        message_to_send = "<" + addr[0] + "> " + conf.RED + conf.PRIVATE_ERROR + Style.RESET_ALL
                        self.jsonSend(conn, message_to_send)

                    elif data[:2] == "cd" and data[3:] == "général":
                        chanel_id = 2
                        self.clientList.append(conn)
                        self.clientIPList.append(addr)
                        self.jsonSend(conn, conf.WHITE + conf.INFO_CHANEL + "général\n" + Style.RESET_ALL)
                        while True:
                            msg = self.jsonRecv(conn)
                            if msg == "@exit":
                                self.jsonSend(conn, "\n" + conf.DARK_GREEN + conf.PANEL + Style.RESET_ALL)
                                self.removeClient(conn)
                                break
                            if msg == "@screen":
                                print('screen effectué : ' + addr[0])
                            if msg == "@readFile":
                                print('readFile effectué : ' + addr[0])
                            if msg == "@graphicmod":
                                userInterface.graphicOverload()
                            if msg == "@ascii":
                                message_ascii = choice(conf.LIST_ASCII)
                                message_to_send = "<" + addr[0] + "> " + message_ascii
                                self.adminBroadcast(message_to_send, conn)
                                self.jsonSend(conn, message_ascii + '\n')
                            else:
                                message_to_send = "<" + addr[0] + "> " + msg
                                crypted_msg = self.ccryptor.symEncrypt(message_to_send)
                                print(crypted_msg)
                                self.clientBroadcast(message_to_send, conn)
                                self.caapes_DB.insertMessage(chanel_id, crypted_msg)
                    else:
                        cmd_error = "[!] commande inconnue"
                        message_to_send = "<" + addr[0] + "> " + cmd_error
                        self.jsonSend(message_to_send)
                        # self.remove(conn)
                except:
                    continue
        else:
            self.jsonSend(conn, conf.YELLOW + conf.LOGIN_ERROR + Style.RESET_ALL)
            self.remove(conn)

    # monitor each connection by appending each client in a list and launch the thread for each client.
    def connectionMon(self):
        # global clientnbr
        while True:
            # try:
            client, addr = self.server.accept()
            print("[+] " + addr[0] + " connected")
            start_new_thread(self.initSession, (client, addr))
            # except:
            #   pass

    # conn.close()
    # server.close()

    # clientnbr = 0

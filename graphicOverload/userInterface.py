# Import Tkinter libraries
from graphicOverload.conf import generalInterfaceConf
from machineLearning import cassandra
from tkinter import *


def graphicOverload():
    def send():
        msg = entry_box.get("1.0", 'end-1c').strip()
        entry_box.delete("0.0", END)

        if msg != '':
            chat_log.config(state=NORMAL)
            chat_log.insert(END, "<Vous> " + msg + '\n')

            #res = 'xx' + msg
            res = cassandra.chatbot_response(msg)
            chat_log.insert(END, "<Cassandra> " + res + '\n')

            chat_log.config(state=DISABLED)
            chat_log.yview(END)

    # Display the window
    window = Tk()
    # Define window title
    window.title("CAAPES")
    # Define the window background
    window['bg'] = generalInterfaceConf.WINDOW_BACKGROUND
    # Define the windows size
    window.geometry(str(generalInterfaceConf.CHAT_PANEL_WIDTH) + "x" + str(generalInterfaceConf.CHAT_PANEL_HEIGHT) + "+"
                    + str(generalInterfaceConf.WINDOW_MARGIN_TOP) + "+" + str(generalInterfaceConf.WINDOW_MARGIN_LEFT))
    # Avoid windows resize
    window.resizable(width=FALSE, height=FALSE)

    # Create Chat window
    chat_log = Text(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                    fg=generalInterfaceConf.CHAT_FONT_COLOR,font=generalInterfaceConf.CHAT_FONT,
                    selectbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING,
                    cursor=generalInterfaceConf.CUSTOM_CURSOR)
    # Avoid to write text
    chat_log.config(state=DISABLED)

    # Bind scrollbar to Chat window
    scrollbar = Scrollbar(window, command=chat_log.yview, cursor=generalInterfaceConf.CUSTOM_CURSOR)
    # Allow to use scrollbar only when it's necessary
    chat_log['yscrollcommand'] = scrollbar.set

    # Create Button to send message
    send_button = Button(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                         fg=generalInterfaceConf.CHAT_FONT_COLOR, font=generalInterfaceConf.CHAT_FONT_BUTTON,
                         activebackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING,
                         cursor=generalInterfaceConf.CUSTOM_CURSOR,
                         text="Envoyer", command=send)

    # Create the box to enter message
    entry_box = Text(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                     fg=generalInterfaceConf.CHAT_FONT_COLOR, font=generalInterfaceConf.CHAT_FONT,
                     selectbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING, insertwidth=5,
                     insertbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING)

    # Place all components on the screen
    scrollbar.place(x=generalInterfaceConf.CHAT_PANEL_WIDTH - (
            generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT + generalInterfaceConf.CHAT_SCROLLBAR_WIDTH),
                    y=generalInterfaceConf.WINDOW_MARGIN,
                    height=((generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO) -
                            generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP))
    chat_log.place(x=generalInterfaceConf.WINDOW_MARGIN,
                   y=generalInterfaceConf.WINDOW_MARGIN,
                   height=(generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO),
                   width=generalInterfaceConf.CHAT_PANEL_WIDTH - (
                           generalInterfaceConf.WINDOW_MARGIN * 2 + generalInterfaceConf.CHAT_SCROLLBAR_WIDTH +
                           generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT))
    entry_box.place(x=generalInterfaceConf.WINDOW_MARGIN,
                    y=((generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO) +
                       generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP),
                    height=(generalInterfaceConf.CHAT_PANEL_HEIGHT - (
                            2 * generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP +
                            (generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO))),
                    width=(generalInterfaceConf.CHAT_PANEL_WIDTH - (
                            generalInterfaceConf.CHAT_BUTTON_WIDTH + 2 * generalInterfaceConf.WINDOW_MARGIN + 2 *
                            generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP)))
    send_button.place(x=(generalInterfaceConf.CHAT_PANEL_WIDTH - (
            generalInterfaceConf.CHAT_BUTTON_WIDTH + generalInterfaceConf.WINDOW_MARGIN +
            generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP)),
                      y=((generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO) +
                         generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP),
                      height=(generalInterfaceConf.CHAT_PANEL_HEIGHT - (
                              2 * generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP +
                              (generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO))),
                      width=generalInterfaceConf.CHAT_BUTTON_WIDTH)

    window.mainloop()

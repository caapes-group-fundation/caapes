from graphicOverload import userInterface

###############################
# Configure screen and panels #
###############################

# Chat panel size
CHAT_PANEL_WIDTH = 950
CHAT_PANEL_HEIGHT = 450
# Admin panel size
ADMIN_PANEL_WIDTH = 550
ADMIN_PANEL_HEIGHT = CHAT_PANEL_HEIGHT
# Window size
WINDOW_WIDTH = CHAT_PANEL_WIDTH + ADMIN_PANEL_WIDTH
WINDOW_HEIGHT = CHAT_PANEL_HEIGHT

# Window margins on screen
WINDOW_MARGIN_TOP = 0
WINDOW_MARGIN_LEFT = 0

# In window margins
WINDOW_MARGIN = 6

# Configure window background
WINDOW_BACKGROUND = 'gray15'

# Configure the cursor
CUSTOM_CURSOR = 'arrow'

###############################
# Configure chat panel        #
###############################

# Configure chat button
CHAT_BUTTON_WIDTH = 100
# Configure chat scrollbar
CHAT_SCROLLBAR_WIDTH = 15

# Configure the ration ChatLog/EntryBox
CHAT_PANEL_WINDOW_RATIO = 1.2

# Configure chat margins
CHAT_COMPONENT_MARGIN_TOP = 2
CHAT_COMPONENT_MARGIN_LEFT = 10

# Configure chat fonts
CHAT_FONT_COLOR = 'red3'
CHAT_FONT_FAMILY = 'CourierNew'
CHAT_FONT_SIZE = '10'
CHAT_FONT_WEIGHT = 'bold'
FONT_SPACE = ' '

CHAT_FONT = CHAT_FONT_FAMILY + FONT_SPACE + CHAT_FONT_SIZE
CHAT_FONT_BUTTON = CHAT_FONT + FONT_SPACE + CHAT_FONT_WEIGHT

# Configure chat backgrounds
CHAT_FONT_HIGHLIGHTING = 'red'
CHAT_COMPONENT_BACKGROUND = 'black'

###############################
# Configure Admin panel       #
###############################

# Configure Admin buttons sizes
# Width Minimum 4
NUMBER_OF_BUTTON = 4
# Height Minimum 5
NUMBER_OF_LINES = 7

# Configure margins
ADMIN_COMPONENT_MARGIN_TOP = 5
ADMIN_COMPONENT_MARGIN_LEFT = CHAT_COMPONENT_MARGIN_LEFT

# Configure Admin panel fonts
ADMIN_FONT_COLOR = "gray3"
ADMIN_FONT_FAMILY = 'CourierNew'
ADMIN_FONT_SIZE = "8"
ADMIN_FONT_WEIGHT = 'bold'

ADMIN_FONT_BUTTON = ADMIN_FONT_FAMILY + FONT_SPACE + ADMIN_FONT_SIZE + FONT_SPACE + ADMIN_FONT_WEIGHT

# Configure Admin background colors
ADMIN_COMPONENT_BACKGROUND = 'gray40'
ADMIN_COMPONENT_HIGHLIGHTING = 'gray30'

# -*- coding: utf-8 -*-
from tkinter import *
from graphicOverload.conf import generalInterfaceConf


def graphicOverload():
    def send():
        msg = entry_box.get("1.0", 'end-1c').strip()
        entry_box.delete("0.0", END)

        if msg != '':
            chat_log.config(state=NORMAL)
            chat_log.insert(END, "<Vous> " + msg + '\n')

            # res = chatbot_response(msg)
            res = msg
            chat_log.insert(END, "<Cassandra> " + res + '\n')

            chat_log.config(state=DISABLED)
            chat_log.yview(END)

    def action1():
        chat_log.config(state=NORMAL)
        chat_log.insert(END, "<Window> [!] Option 1 executed\n")
        chat_log.config(state=DISABLED)
        chat_log.yview(END)

    def action2():
        chat_log.config(state=NORMAL)
        chat_log.insert(END, "<Window> [!] Option 2 executed\n")
        chat_log.config(state=DISABLED)
        chat_log.yview(END)

    def action3():
        chat_log.config(state=NORMAL)
        chat_log.insert(END, "<Window> [!] Option 3 executed\n")
        chat_log.config(state=DISABLED)
        chat_log.yview(END)

    def action4():
        chat_log.config(state=NORMAL)
        chat_log.insert(END, "<Window> [!] Option 4 executed\n")
        chat_log.config(state=DISABLED)
        chat_log.yview(END)

    def action5():
        chat_log.config(state=NORMAL)
        chat_log.insert(END, "<Window> [!] Option 5 executed\n")
        chat_log.config(state=DISABLED)
        chat_log.yview(END)

    def action6():
        chat_log.config(state=NORMAL)
        chat_log.insert(END, "<Window> [!] Option 6 executed\n")
        chat_log.config(state=DISABLED)
        chat_log.yview(END)

    # Display the window
    window = Tk()
    # Define window title
    window.title("CAAPES")
    # Define the window background
    window['bg'] = generalInterfaceConf.WINDOW_BACKGROUND
    # Define the windows size
    window.geometry(str(generalInterfaceConf.WINDOW_WIDTH) + "x" + str(generalInterfaceConf.WINDOW_HEIGHT) + "+"
                    + str(generalInterfaceConf.WINDOW_MARGIN_TOP) + "+" + str(generalInterfaceConf.WINDOW_MARGIN_LEFT))
    # Avoid window resize
    window.resizable(width=FALSE, height=FALSE)

    # Create Chat window
    chat_log = Text(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                    fg=generalInterfaceConf.CHAT_FONT_COLOR, font=generalInterfaceConf.CHAT_FONT,
                    selectbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING,
                    cursor=generalInterfaceConf.CUSTOM_CURSOR)
    # Avoid to write text
    chat_log.config(state=DISABLED)

    # Bind scrollbar to Chat window
    scrollbar = Scrollbar(window, command=chat_log.yview, cursor=generalInterfaceConf.CUSTOM_CURSOR)
    # Allow to use scrollbar only when it's necessary
    chat_log['yscrollcommand'] = scrollbar.set

    # Create Button to send message
    send_button = Button(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                         fg=generalInterfaceConf.CHAT_FONT_COLOR, font=generalInterfaceConf.CHAT_FONT_BUTTON,
                         activebackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING,
                         cursor=generalInterfaceConf.CUSTOM_CURSOR,
                         text="Envoyer", command=send)

    # Create the box to enter message
    entry_box = Text(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                     fg=generalInterfaceConf.CHAT_FONT_COLOR, font=generalInterfaceConf.CHAT_FONT,
                     selectbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING, insertwidth=5,
                     insertbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING)

    # Instantiate Admin panel buttons
    # Will be transformed into a for loop to auto generate buttons in a future release
    function1_button = Button(window, bd=1, bg=generalInterfaceConf.ADMIN_COMPONENT_BACKGROUND,
                              fg=generalInterfaceConf.ADMIN_FONT_COLOR, font=generalInterfaceConf.ADMIN_FONT_BUTTON,
                              activebackground=generalInterfaceConf.ADMIN_COMPONENT_HIGHLIGHTING,
                              cursor=generalInterfaceConf.CUSTOM_CURSOR,
                              text="Option 1", command=action1)
    function2_button = Button(window, bd=1, bg=generalInterfaceConf.ADMIN_COMPONENT_BACKGROUND,
                              fg=generalInterfaceConf.ADMIN_FONT_COLOR, font=generalInterfaceConf.ADMIN_FONT_BUTTON,
                              activebackground=generalInterfaceConf.ADMIN_COMPONENT_HIGHLIGHTING,
                              cursor=generalInterfaceConf.CUSTOM_CURSOR,
                              text="Option 2", command=action2)
    function3_button = Button(window, bd=1, bg=generalInterfaceConf.ADMIN_COMPONENT_BACKGROUND,
                              fg=generalInterfaceConf.ADMIN_FONT_COLOR, font=generalInterfaceConf.ADMIN_FONT_BUTTON,
                              activebackground=generalInterfaceConf.ADMIN_COMPONENT_HIGHLIGHTING,
                              cursor=generalInterfaceConf.CUSTOM_CURSOR,
                              text="Option 3", command=action3)
    function4_button = Button(window, bd=1, bg=generalInterfaceConf.ADMIN_COMPONENT_BACKGROUND,
                              fg=generalInterfaceConf.ADMIN_FONT_COLOR, font=generalInterfaceConf.ADMIN_FONT_BUTTON,
                              activebackground=generalInterfaceConf.ADMIN_COMPONENT_HIGHLIGHTING,
                              cursor=generalInterfaceConf.CUSTOM_CURSOR,
                              text="Option 4", command=action4)
    function5_button = Button(window, bd=1, bg=generalInterfaceConf.ADMIN_COMPONENT_BACKGROUND,
                              fg=generalInterfaceConf.ADMIN_FONT_COLOR, font=generalInterfaceConf.ADMIN_FONT_BUTTON,
                              activebackground=generalInterfaceConf.ADMIN_COMPONENT_HIGHLIGHTING,
                              cursor=generalInterfaceConf.CUSTOM_CURSOR,
                              text="Option 5", command=action5)
    function6_button = Button(window, bd=1, bg=generalInterfaceConf.ADMIN_COMPONENT_BACKGROUND,
                              fg=generalInterfaceConf.ADMIN_FONT_COLOR, font=generalInterfaceConf.ADMIN_FONT_BUTTON,
                              activebackground=generalInterfaceConf.ADMIN_COMPONENT_HIGHLIGHTING,
                              cursor=generalInterfaceConf.CUSTOM_CURSOR,
                              text="Option 6", command=action6)

    # Admin Panel Button placement
    function1_button.place(x=generalInterfaceConf.CHAT_PANEL_WIDTH,
                           y=generalInterfaceConf.WINDOW_MARGIN,
                           width=(generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - (
                                 generalInterfaceConf.WINDOW_MARGIN),
                           height=generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES)

    function2_button.place(
        x=generalInterfaceConf.CHAT_PANEL_WIDTH + generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT + ((
          generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) -
          2 * generalInterfaceConf.WINDOW_MARGIN),
        y=generalInterfaceConf.WINDOW_MARGIN,
        width=(generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - (
              generalInterfaceConf.WINDOW_MARGIN),
        height=generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES)

    function3_button.place(
        x=generalInterfaceConf.CHAT_PANEL_WIDTH + 2 * generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT + 2 * ((
          generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - 2 *
          generalInterfaceConf.WINDOW_MARGIN),
        y=generalInterfaceConf.WINDOW_MARGIN,
        width=(generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - (
              generalInterfaceConf.WINDOW_MARGIN),
        height=generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES)

    function4_button.place(
        x=generalInterfaceConf.CHAT_PANEL_WIDTH + 3 * generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT + 3 * (
                (generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - 2 *
                generalInterfaceConf.WINDOW_MARGIN),
        y=generalInterfaceConf.WINDOW_MARGIN,
        width=(generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - (
              generalInterfaceConf.WINDOW_MARGIN),
        height=generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES)

    function5_button.place(
        x=generalInterfaceConf.CHAT_PANEL_WIDTH,
        y=(generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES) + (
          generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.ADMIN_COMPONENT_MARGIN_TOP),
        width=(generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - (
              generalInterfaceConf.WINDOW_MARGIN),
        height=generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES)

    function6_button.place(
        x=generalInterfaceConf.CHAT_PANEL_WIDTH + generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT + ((
          generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) -
          2 * generalInterfaceConf.WINDOW_MARGIN),
        y=(generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES) + (
          generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.ADMIN_COMPONENT_MARGIN_TOP),
        width=(generalInterfaceConf.ADMIN_PANEL_WIDTH / generalInterfaceConf.NUMBER_OF_BUTTON) - (
              generalInterfaceConf.WINDOW_MARGIN),
        height=generalInterfaceConf.ADMIN_PANEL_HEIGHT / generalInterfaceConf.NUMBER_OF_LINES)

    # Create Button to send message
    send_button = Button(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                         fg=generalInterfaceConf.CHAT_FONT_COLOR, font=generalInterfaceConf.CHAT_FONT_BUTTON,
                         activebackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING,
                         cursor=generalInterfaceConf.CUSTOM_CURSOR,
                         text="Envoyer", command=send)

    # Create the box to enter message
    entry_box = Text(window, bd=0, bg=generalInterfaceConf.CHAT_COMPONENT_BACKGROUND,
                     fg=generalInterfaceConf.CHAT_FONT_COLOR,
                     font=generalInterfaceConf.CHAT_FONT,
                     selectbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING, insertwidth=5,
                     insertbackground=generalInterfaceConf.CHAT_FONT_HIGHLIGHTING)

    # Place all components on the screen
    scrollbar.place(x=generalInterfaceConf.CHAT_PANEL_WIDTH - (
            generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT + generalInterfaceConf.CHAT_SCROLLBAR_WIDTH),
                    y=generalInterfaceConf.WINDOW_MARGIN,
                    height=((generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO) -
                            generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP))
    chat_log.place(x=generalInterfaceConf.WINDOW_MARGIN,
                   y=generalInterfaceConf.WINDOW_MARGIN,
                   height=(generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO),
                   width=generalInterfaceConf.CHAT_PANEL_WIDTH - (
                           generalInterfaceConf.WINDOW_MARGIN * 2 + generalInterfaceConf.CHAT_SCROLLBAR_WIDTH +
                           generalInterfaceConf.CHAT_COMPONENT_MARGIN_LEFT))
    entry_box.place(x=generalInterfaceConf.WINDOW_MARGIN,
                    y=((generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO) +
                       generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP),
                    height=(generalInterfaceConf.CHAT_PANEL_HEIGHT - (
                            2 * generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP +
                            (generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO))),
                    width=(generalInterfaceConf.CHAT_PANEL_WIDTH - (
                            generalInterfaceConf.CHAT_BUTTON_WIDTH + 2 * generalInterfaceConf.WINDOW_MARGIN + 2 *
                            generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP)))
    send_button.place(x=(generalInterfaceConf.CHAT_PANEL_WIDTH - (
            generalInterfaceConf.CHAT_BUTTON_WIDTH + generalInterfaceConf.WINDOW_MARGIN +
            generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP)),
                      y=((generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO) +
                         generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP),
                      height=(generalInterfaceConf.CHAT_PANEL_HEIGHT - (
                              2 * generalInterfaceConf.WINDOW_MARGIN + generalInterfaceConf.CHAT_COMPONENT_MARGIN_TOP +
                              (generalInterfaceConf.CHAT_PANEL_HEIGHT / generalInterfaceConf.CHAT_PANEL_WINDOW_RATIO))),
                      width=generalInterfaceConf.CHAT_BUTTON_WIDTH)

    window.mainloop()
